import { useState, useEffect, useContext } from 'react';
import { Container, Card, Button, Row, Col, Form, FloatingLabel} from 'react-bootstrap';
import { useParams, Link } from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';



export default function ProductView() {

	const { productId } = useParams();

	// To be able to obtain the user ID so that a user can order
	const { user } = useContext(UserContext);

	const [ name, setName ] = useState('');
	const [ description, setDescription ] = useState('');
	const [ price, setPrice ] = useState(0);
	const [ quantity, setQuantity ] = useState(0);
	const [isActive, setIsActive] = useState(false);
	
	 


	useEffect(() => {
		console.log(productId);

		// Fetch request that will retrieve the details of the product from the database to be displayed in the "ProductView" page
		fetch(`${process.env.REACT_APP_API_URL}/products/${productId}`)
		.then(res => res.json())
		.then(data => {
			console.log(data);

			setName(data.name);
			setDescription(data.description);
			setPrice(data.price);
		})
	}, [productId]);

	const order = (productId) => {

		fetch(`${process.env.REACT_APP_API_URL}/users/checkOut`, {
			method: "POST",
			headers: {
				"Content-Type": "application/json",
				Authorization: `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				productId: productId,
				quantity: quantity
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data);

			if(data === true) {
				Swal.fire({
					title: "Order successful!",
					icon: "success",
					text: "You have successfully ordered the product!"
				})
			} else {
				Swal.fire({
					title: "Something went wrong",
					icon: "error",
					text: "Please try again."
				})
			}
		})
	}

	const handleDecrement = () => {
		if(quantity > 0){
			setQuantity(quantity - 1)
		}
	}
	

	const handleIncrement = () => {
		if (user.id !== null) {
		 	setQuantity(quantity + 1)
		}
	}


		const shipping = quantity * 460;
		const totalAmount = ((quantity * price) + shipping);
		const [fname, setFname] = useState('');
		const [lname, setLname] = useState('');
		const [address1, setAddress1] = useState('');		
		const [address2, setAddress2] = useState('');
		const [gender, setGender] = useState('');

		useEffect(() => {
			if (fname !== '' && lname !== '' && address1 !== '' && address2 !== '')  {
				setIsActive(true);
			} else {
				setIsActive(false);
			}
		}, [fname, lname, address1, address2]);

   	
	return (

		<Container className="mt-5 " >
			<Row>
				<Col >
					
					<Card >
						<Card.Body className="text-center mb-5">
					        <Card.Title>{name}</Card.Title>
					        <Card.Subtitle>Description</Card.Subtitle>
					        <Card.Text className = "text-justify" >{description}</Card.Text>
					        <Card.Subtitle>Price</Card.Subtitle>
					        <Card.Text>Php {price.toLocaleString("en",{minimumFractionDigits:2})}</Card.Text>
					        <Card.Subtitle>Quantity:</Card.Subtitle>
					         <div className="input-group mt-2 mb-3 justify-content-center" >
					        	<Button variant="primary" onClick={() => handleDecrement(productId)} className="input-group-text">-</Button>
					        	<div className="input-group-text text-center">{quantity}</div>
					        	<Button variant="primary" onClick={() => handleIncrement(productId)} className="input-group-text">+</Button>
					        </div>
					        


					        <div>
					      			  <h5>Preferred Gender</h5>
					              <select id="Gender" 
					              value={gender}
					              onChange={e => setGender(e.target.value)}>
					                <option value="Female">Female</option>
					                <option value="Male">Male</option>
					                <option value="Random">Random</option>
					              </select>
					         </div>


					 </Card.Body>
					 </Card>
				</Col>
				<Col>
					 <Card>
					 <Card.Body>
					        <h2>Order Summary</h2>
					        <Card.Subtitle>{fname} {lname}</Card.Subtitle>
					        <Card.Subtitle>{address1} , {address2}</Card.Subtitle>
					         <Card.Subtitle>{gender}</Card.Subtitle>
					         <Card.Subtitle>Shipping fee : 460.00/kitten</Card.Subtitle>
					         <hr></hr>
					         <Card.Subtitle><i>Note: Every order comes with a separate cage to guarantee the safety of the kittens.</i></Card.Subtitle>
					          <hr></hr>
					         <Card.Subtitle>Shipping Total : {shipping.toLocaleString("en",{minimumFractionDigits:2})} </Card.Subtitle>
					        <Card.Subtitle>Total Amount Purchase</Card.Subtitle>
					    	<Card.Subtitle>Php {totalAmount.toLocaleString("en",{minimumFractionDigits:2})}</Card.Subtitle>
					    					       
					        
					       
						</Card.Body>
					    {user.id !== null && isActive === true
					    	?
					    	<Button variant="primary" onClick={() => order(productId)}>Checkout</Button>
					    	:
					    	<Button as={Link} to="/login" variant="danger">Please Input Required Fields</Button>
						}
					</Card>
				</Col>
				<Form>
				      <Row className="mb-3">
				        <Form.Group as={Col} controlId="formGridEmail">
				        <FloatingLabel
				        	   controlId="floatingfname"
                               label="First Name
                               " className="mt-5">
				          <Form.Control type="text" className ="text-dark" 
				          placeholder="First Name" 
				          required 
				          value={fname}
					onChange={e => setFname(e.target.value)} />
					</FloatingLabel>
				        </Form.Group>

				        <Form.Group as={Col} controlId="formGridPassword">
				       <FloatingLabel
				        	   controlId="floatinglname"
                               label="Last Name
                               " className="mt-5">
				          <Form.Control 
				          type="lname" 
				          placeholder="Last Name"
				          value={lname}
					onChange={e => setLname(e.target.value)} />
					</FloatingLabel	>
				        </Form.Group>
				      </Row>

				      <Form.Group className="mb-3" controlId="formGridAddress1">
				     <FloatingLabel
				        	   controlId="floatingAdd1"
                               label="House #, Street, Subdivision, Barangay
                               " className="mt-5">
				        <Form.Control
				         placeholder="1234 Main St,Eco Subdivision, Brgy. 123"
				         value={address1}
				         onChange={e => setAddress1(e.target.value)} />
				         </FloatingLabel>
				      </Form.Group>

				      <Form.Group className="mb-3" controlId="formGridAddress2">
				        <FloatingLabel
				        	   controlId="floatingAdd2"
                               label="City/Municipality, Province
                               " className="mt-5">
				        <Form.Control placeholder="Apartment, studio, or floor" 
				        value={address2}
				        onChange={e => setAddress2(e.target.value)}/>
				        </FloatingLabel>
				      </Form.Group>


				    </Form>


			</Row>
		</Container>
	)
}