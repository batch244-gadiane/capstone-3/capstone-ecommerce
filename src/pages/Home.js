	import { Container, Card	} from 'react-bootstrap';
import Banner from '../components/Banner';
import HomeCard from '../components/HomrCard';
/*import Jump from "react-reveal/Jump";
import BackgroundSlider from "react-background-slider"
import React from "react";
*/

export default function Home() {

	const data = {
		title: "House of Cats",
		content: "If kittens are out of your price range, you also can't afford cat food, supplements, and other essentials. ",
		otherContent: "Prior to releasing kittens, we interview potential buyers!",
		destination: "/products",
		label: "Adopt Now!"
	}

	
    

	return (
		<>
			
			<div className	= "homepage">
	          <Banner data={data}/>
	          </div>
	          <div>
			<HomeCard/>   
			 </div>     	  
        </>
	)
}