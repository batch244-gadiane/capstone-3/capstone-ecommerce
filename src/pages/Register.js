import { useState, useEffect, useContext } from 'react';
import { Form, Button, FloatingLabel, Card} from 'react-bootstrap';
import { Navigate, useNavigate } from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';



export default function Register() {

	const {user} = useContext(UserContext);

	const navigate = useNavigate();
	const[firstName, setFirstName] = useState ('');
	const[lastName, setLastName] = useState ('');	
	const [email, setEmail] = useState('');
	const [password1, setPassword1] = useState('');
	const [password2, setPassword2] = useState('');	

	const [isActive, setIsActive] = useState(false);

	


	function registerUser(e) {

		e.preventDefault();
		fetch(`${process.env.REACT_APP_API_URL}/users/checkEmail`, {
			method: "POST",
			headers: {
				"Content-Type" : "application/json"
			},
			body: JSON.stringify({
				email: email,

			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data);

			if (data === true) {

				Swal.fire({
					title: "Duplicate email found!",
					icon: "error",
					text: "Kindly provide another email to complete the registration."
				})

			} else {

		fetch(`${process.env.REACT_APP_API_URL}/users/register`, {
			method: "POST",
			headers: {
				"Content-Type": "application/json"
			},
			body: JSON.stringify({
				email: email,
				firstName: firstName,	
				lastName : lastName,
				password: password1
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data);

			if (data === true){
				Swal.fire({
					title: "Registration successful",
					icon: "success",
					text: "Thankyou for Registeration! Happy Shopping !"
				})

				setEmail('');
				setPassword1('');
				setPassword2('');

			} else {
				Swal.fire({
					title: "User already exists",
					icon: "error",
					text: "Please provide another email to complete the registration."
					})
					};
					navigate("/login");
				})
			}
		})	

		// alert('Thank you for registering!');
	}
	useEffect(() => {
		if ((email !== '' && password1 !== '' && password2 !== '') && (password1 === password2)) {
			setIsActive(true);
		} else {
			setIsActive(false);
		}
	}, [email, password1, password2]);

	return (
		(user.id !== null)
		?
			<Navigate to="/products"/>
			
		:


	<Card className = "Cardregister col-lg-4 col-md-4 col-sm-12">
	 <img className = "imageRegister image-fluid" src ="https://wallpapers.com/images/featured/13sja0rf4w9sbbr6.jpg" alt=''/>
		<Form className= "formregister" onSubmit={(e) => registerUser(e)}>
			<Form.Group controlId = "fname">
				<FloatingLabel
		              		 controlId="floatingfname"
		                       label="First Name"
		                       className="mb-3">
				<Form.Control 
					type="text"
					placeholder="First Name"
					required
					value={firstName}
					onChange={e => setFirstName(e.target.value)}
				/>
				</FloatingLabel>
			</Form.Group>
			<Form.Group controlId = "lname">
				<FloatingLabel
		              		 controlId="floatinglname"
		                       label="Last Name"
		                       className="mb-3">
				<Form.Control 
					type="text"
					placeholder="Last Name"
					required
					value={lastName}
					onChange={e => setLastName(e.target.value)}
				/>
				</FloatingLabel>
			</Form.Group>


			<Form.Group controlId = "userEmail">
				<FloatingLabel
		              		 controlId="floatingEmail"
		                       label="Email Address"
		                       className="mb-3">
				<Form.Control 
					type="email"
					placeholder="Email Address"
					required
					value={email}
					onChange={e => setEmail(e.target.value)}
				/>
				</FloatingLabel>
			</Form.Group>

			<Form.Group controlId = "password1">
				<FloatingLabel
		              		 controlId="floatingPassword1"
		                       label="Password"
		                       className="mb-3">
				<Form.Control 
					type="password"
					placeholder="Password"
					required
					value={password1}
					onChange={e => setPassword1(e.target.value)}
				/>
				</FloatingLabel>
			</Form.Group>

			<Form.Group className="mb-3" controlId = "password2">
				<FloatingLabel
		              		 controlId="floatingPassword2"
		                       label="Verify Password"
		                       className="mb-3">
				<Form.Control					 
					type="password"
					placeholder="Verify Password"
					required
					value={password2}
					onChange={e => setPassword2(e.target.value)}
				/>
				</FloatingLabel>
			</Form.Group>
			
			{isActive
				?
					<Button variant="secondary outline-light" type="submit" id="submitBtn">Submit</Button>
				: 	
					<Button variant="danger outline-dark" type="submit" id="submitBtn" disabled>Submit</Button>
			}
		</Form>

	</Card>
	)
}