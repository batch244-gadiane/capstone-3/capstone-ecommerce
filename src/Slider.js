import React from "react";
// import Slider from "react-slick";
import slider1 from "../images/3.jpg";
import slider2 from "../images/2.jpg";
import slider4 from "../images/cat.jpg";

import BackgroundSlider from 'react-background-slider'


const ImageSlider = () => {
  
  return (
    <>
      

<BackgroundSlider
  images={[slider1,slider2,slider4]}
  duration={15} transition={2} />

<div className="banner">
              <div className="item">
           
                <div className="banner-content">
                  <h1 className="banner-title ">
                    AYDIN Co For Framing , Package and delivery
                  </h1>
                  <a href="/chmdemo/AydinCo/Shop">
                    <button class="heroButton">
                      <h2>Shop Now</h2>
                    </button>
                  </a>
                </div>
              </div>
            </div>
    </>
  );
};

export default ImageSlider;