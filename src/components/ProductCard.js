import { Card, Button , Row, Col} from 'react-bootstrap';
import { Link } from 'react-router-dom';


export default function ProductCard({productProp}) {

	const { _id, name, description, price,  } = productProp;
	

	return (	
	
		
		<div className = "row" >
			<Col className = "p-3 m-1">	
		<a href={`/productView/${_id}`}>
				<Card className =" text-center alignItems-center productCard2">
				    <Card.Body className= "productCard-design">
				        <Card.Title>{name}</Card.Title>
				        <Card.Subtitle>Description:</Card.Subtitle>
				        <Card.Text>{description}</Card.Text>
				        
				        <Card.Subtitle>Price:</Card.Subtitle>
				        <Card.Text>Php {price.toLocaleString("en",{minimumFractionDigits:2})}</Card.Text>
				        <Button as={Link} variant="primary" to={`/products/${_id}`}>Details</Button>
				    </Card.Body>
				</Card>
		</a>			
			</Col>		
		</div>
		
	)
}