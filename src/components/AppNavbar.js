import { useContext } from 'react';
import { Navbar, Container, Nav} from 'react-bootstrap';
import { Link, NavLink } from 'react-router-dom';
import UserContext from '../UserContext';

export default function AppNavbar() {

  const { user } = useContext(UserContext);

  // State to store the user information stored in the login page
  // const [user, setUser] = useState(localStorage.getItem('email'));
  // console.log(user);

	return(
		<Navbar className = "navbar-default" expand ="lg"  >
        <Container>
        <img src ="https://cdn.dribbble.com/users/1088267/screenshots/3678500/cat-800x600.gif" alt="logo" style={{ width: '50px', }}/>
          <Navbar.Brand className = "text-white" as={Link} to="/">Kitten Store</Navbar.Brand>
          <Navbar.Toggle aria-controls="basic-navbar-nav" />
          <Navbar.Collapse id="basic-navbar-nav">
            <Nav className="me-auto">
              <Nav.Link className = "text-white" as={NavLink} to="/">Home</Nav.Link>
              
              {
                (user.isAdmin)
                ?
                <Nav.Link className = "text-white" as={ NavLink } to="/admin" end>Admin Dashboard</Nav.Link>
                :
                <Nav.Link className = "text-white" as={ NavLink } to="/products" end>Products</Nav.Link>
              }

              {/*Conditional rendering to such that the Logout link will be shown instead of the Login and Register when a user is logged in*/}
              {(user.id !== null)

                ?
                <Nav.Link className = "text-white" as={NavLink} to="/logout">Logout</Nav.Link>

                :
                <>
                  <Nav.Link className = "text-white" as={NavLink} to="/login">Login</Nav.Link>
                  <Nav.Link className = "text-white" as={NavLink} to="/register">Register</Nav.Link> 
                </>
              }

            </Nav>
          </Navbar.Collapse>
        </Container>
      </Navbar>
	)
}

// export default AppNavbar (can also be used)